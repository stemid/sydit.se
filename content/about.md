---
title: "About"
date: 2022-12-05T07:38:44+01:00
---


SydIT is a Swedish 🇸🇪 ["Enskild näringsverksamhet"](https://en.wikipedia.org/wiki/Sole_proprietorship) type of business, registered for Swedish VAT called ["FA-skatt"](https://en.wikipedia.org/wiki/Swedish_F-tax_certificate), sole proprietor [Stefan Midjich](https://swehack.se).

With a focus on open source software, SydIT can solve tough problems for your IT organization without costing a fortune in license fees.

Decades of experience designing and building platforms on Linux and BSD Unix, with a very opinionated view of security, functionality and usability.
