---
title: "Services"
date: 2022-12-05T07:23:21+01:00
---

# Consulting services

See [the Contact page](/contact) for how to contact SydIT about consulting in;

* [BSD & Linux](#bsd--linux)
* [Open-source licensing for enterprise](#open-source-licensing-for-enterprise)
* [Security hardening & PKI](#security-hardening--pki)
* [Software architecture design](#software-architecture-design)
* [Distributed systems](#distributed-systems)
* [Devops & Automation](#devops--automation)
* [Static site hosting](#static-site-hosting)
* [Cloud integration](#cloud-integration)
* [Systems integration](#systems-integration)
* [Observability](#observability)

## BSD & Linux

Red Hat certified since 2014, and 20+ years experience using and setting up BSD and Linux flavors like OpenBSD, FreeBSD, Debian, Ubuntu, CentOS, RHEL and Fedora.

The latest trend is to use immutable operating systems that eliminate a lot of human error by decreasing the need for old school sysadmins to login and make manual changes in the operating system.

But it's important not to be biased, so [contact SydIT](/contact) to find out if BSD, Linux, or Microsoft Windows, is right for your use-case.

## Open-source licensing for enterprise

* You want to use open-source software but afraid of licensing restrictions?

[Contact SydIT](/contact) to help straighten out all your worries.

## Security hardening & PKI

* Want to harden your Linux systems according to known security standards like CISSP?
* Want to sandbox your applications with SElinux?
* Want to setup an air-gapped PKI on Linux?

[Contact SydIT](/contact) for consulting.

## Software architecture design

* Want help designing complex software from scratch?

[Contact SydIT](/contact) for consulting.

## Distributed systems

Consulting in several degrees of distributed systems, from complex and resource hungry Kubernetes, to light and flexible Podman Quadlets with shared state in Memcache or Redis for example.

Many years of experience setting up and running primary/standby, or multi-primary, database clusters with PostgreSQL and MariaDB.

## Devops & Automation

Devops is just like IT-support for developers. Much like end users, developers need an intuitive and patient single point of contact between them and operations.

Infrastructure as Code is the key to modern operations. Don't just blindly automate all the things because it's hyped, get help identifying the most efficient and user friendly solution for your use-case.

## Static site hosting

* Simple site hosting should not cost more than a few dollars a month in the cloud.
* Light dynamic features like contact forms can be added.
* Price scales with traffic.

[Contact SydIT](/contact) for consulting.

## Cloud integration

* Leverage cloud services for drastic cost savings in your on-prem environments.
* Create hybrid environments with a focus on security by hosting the critical parts on-prem, and the less critical parts in the cloud.

[Contact SydIT](/contact) for consulting.

## Systems integration

* Have a lot of expensive systems that don't interact?
* Want help purchasing new systems that can integrate with your existing systems?
* Want to integrate open-source ticketing systems, monitoring systems with enterprise process management systems?

[Contact SydIT](/contact) for consulting.

## Observability

* Catch incidents before they happen with tracing.
* Alert your 1st line 24/7 with monitoring.
* Help your devops find bottlenecks and security issues with logging.

[Contact SydIT](/contact) for consulting.
